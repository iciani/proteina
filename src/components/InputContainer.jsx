import * as React from 'react'
import { TextField } from '@mui/material'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import FormHelperText from '@mui/material/FormHelperText'
import '../styles.css'

function InputContainer ({ tipo, variant, id, name, label, value, helperText, error, handleChange, selectValues }) {
  return (
    <>
      {tipo === 'text' &&
          <TextField
          variant={variant}
          fullWidth
          id={id}
          name={name}
          label={label}
          value={value}
          onChange={handleChange}
          error={error}
          helperText={helperText}
          />
      }

      {tipo === 'select' &&
        <div>
            <FormControl fullWidth variant={variant} error={error}>
                <InputLabel id={name}>{ label }</InputLabel>
                <Select labelId={name} id={id} name={name} onChange={handleChange} value={value !== 'undefined' ? value : '' }>
                        {selectValues && selectValues.map((option) => (
                          <MenuItem key={option.value} value={option.value}>{option.text}</MenuItem>
                        ))}
                </Select>
                <FormHelperText>{helperText}</FormHelperText>
            </FormControl>
        </div>
      }
    </>
  )
}

export default InputContainer
