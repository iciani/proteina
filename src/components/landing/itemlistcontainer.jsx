import React, { useState, useEffect } from 'react'
import { Grid } from '@mui/material'
import IconButton from '@mui/material/Button'
import ArrowDropDownCircleTwoToneIcon from '@mui/icons-material/ArrowDropDownCircleTwoTone'
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore'
import Title from './Title'
import ItemList from './ItemList'
import '../../styles.css'

const ItemListContainer = ({ greeting, inicio, setCurrentItem }) => {
  const [productos, setProductos] = useState([])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    setLoading(true)
    const db = getFirestore()
    const itemCollection = query(collection(db, 'productos'), where('categoria', '==', greeting))

    getDocs(itemCollection).then((snapshot) => {
      if (snapshot.size === 0) {
        console.log('no results!')
      }
      setProductos(snapshot.docs.map((producto) => ({ ...producto.data(), id: producto.id })))
    }).catch(
      // (error) => {
      // console.log('Error al conectar con firebase en ItemListContainer.'.error)
      // throw new Error('Whoops!')
      // }
    ).finally(res => {
      setLoading(false)
    })
  }, [greeting])

  return (
        <>
            {inicio &&
            <div className="welcome">
                <IconButton color="primary" aria-label="Go to Products" >
                    <a className="noLinkButton" href={'#lista'}><ArrowDropDownCircleTwoToneIcon size="170" /></a>
                </IconButton>
            </div>
            }
            <div id="lista">
                <Grid item sx={{ pt: 5, mb: 10 }} xs={12} sm={12} md={12} lg={12}>
                    <Title name={greeting}/>
                </Grid>
                <ItemList productos={productos} loading={loading} setCurrentItem={setCurrentItem} />
            </div>
        </>
  )
}

export default ItemListContainer
