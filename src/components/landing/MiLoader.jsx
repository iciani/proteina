import React from 'react'
import ContentLoader from 'react-content-loader'

const MiLoader = (props) => (
  <ContentLoader
    speed={2}
    width={345}
    height={445}
    viewBox="0 0 345 445"
    backgroundColor="#fff3c9"
    foregroundColor="#ecebeb"
    {...props}
  >
    <rect x="8" y="205" rx="3" ry="3" width="238" height="16" />
    <rect x="9" y="240" rx="3" ry="3" width="140" height="19" />
    <rect x="9" y="284" rx="3" ry="3" width="338" height="19" />
    <rect x="9" y="330" rx="3" ry="3" width="149" height="43" />
    <rect x="196" y="330" rx="3" ry="3" width="149" height="43" />
    <rect x="10" y="386" rx="3" ry="3" width="337" height="43" />
    <rect x="9" y="6" rx="3" ry="3" width="337" height="180" />
  </ContentLoader>
)

export default MiLoader
