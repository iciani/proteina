import * as React from 'react'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import { useState, useContext } from 'react'
import { Grid } from '@mui/material'
import { Link } from 'react-router-dom'
import NumberFormat from 'react-number-format'

import { CartContext } from '../../Context/CartContext'
import Badge from '@mui/material/Badge'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

export default function ItemCount ({ id, name, alt, precio, description, categoria, imagen, stock, initial, setCurrentItem }) {
  const context = useContext(CartContext)
  const [count, setCount] = useState(initial)

  withReactContent(Swal)

  const addItem = () => {
    const newValue = parseInt(count) + parseInt(1)
    if (newValue <= stock) {
      setCount(parseInt(newValue))
    }
  }

  const quitItem = () => {
    if (count > initial) {
      setCount(parseInt(count - 1))
    }
  }

  const onAdd = () => {
    context.addItem(id, name, alt, precio, parseInt(count))
    setCount(parseInt(1))

    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Producto agregado al Carrito.',
      showConfirmButton: false,
      timer: 750
    })
  }

  return (
    <Card sx={{ maxWidth: 345 }}>
      <Link to={`/item/${id}`}>
        <CardMedia
          className="clickeable"
          component="img"
          alt={alt}
          height="140"
          image={imagen}
          sx={{ mt: 2 }}
          onClick={() => setCurrentItem(id)}
        />
      </Link>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {name}<br />{alt}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {description}<br />{categoria}
        </Typography>
        <h2 style={{ margin: 0 }} >
          { categoria === 'Ofertas de la Semana!'
            ? <Badge badgeContent={'Oferta!'} color="error" size="large" anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} >
            <NumberFormat value={precio}
                          displayType={'text'}
                          thousandSeparator={true}
                          prefix={'$'}
                          renderText={(value, props) => <div {...props}>{value}</div>} />
          </Badge>
            : <NumberFormat value={precio}
                          displayType={'text'}
                          thousandSeparator={true}
                          prefix={'$'}
                          renderText={(value, props) => <div {...props}>{value}</div>} />
          }
        </h2>

        <Typography sx={{ pt: '5px' }} variant="body2" color="text.secondary">Stock: ({stock})</Typography>
      </CardContent>
      <Grid container direction="row" justifyContent="center" alignItems="center" spacing={0}>
          <Button onClick={addItem} variant='contained' color='secondary' sx={{ p: 1, width: '40%' }} size="small"><b>+</b></Button>
          <h2>{count}</h2>
          <Button onClick={quitItem} variant='contained' color='secondary' sx={{ p: 1, width: '40%' }} size="small"><b>-</b></Button>
      </Grid>
      <Button onClick={onAdd} variant='contained' sx={{ m: 1, width: '85%' }} size="medium">Agregar</Button>
    </Card>
  )
}
