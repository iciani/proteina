import React, { useState, useContext } from 'react'
import { Grid, Paper } from '@mui/material'
import Typography from '@mui/material/Typography'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import Box from '@mui/material/Box'

import NumberFormat from 'react-number-format'
import Button from '@mui/material/Button'

import { CartContext } from '../../Context/CartContext'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

function TabPanel (props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

function a11yProps (index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  }
}

function ItemDetail ({ id, name, alt, imagen, imagen2, imagen3, stock, initial, description, descriptionLong, categoria, precio }) {
  const context = useContext(CartContext)
  const [value, setValue] = useState(0)
  const [count, setCount] = useState(1)
  withReactContent(Swal)
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const addItem = () => {
    const newValue = +count + +1
    if (newValue <= stock) {
      setCount(newValue)
    }
  }

  const quitItem = () => {
    if (count > initial) {
      setCount(count - 1)
    }
  }

  const onAdd = () => {
    context.addItem(id, name, alt, precio, count)
    setCount(1)

    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Producto agregado al Carrito.',
      showConfirmButton: false,
      timer: 1500
    })
  }

  return (
    <div>
        <Grid container sx={{ p: '25px', height: '720px' }}>
          <Grid className="itemDetailFotoHide" sx={{ width: '50%' }} item>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
              <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                <Tab label="Foto 1" {...a11yProps(0)} />
                <Tab label="Foto 2" {...a11yProps(1)} />
                <Tab label="Foto 3" {...a11yProps(2)} />
              </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
              <img style={{ height: '500px' }} src={`/${imagen}`} alt={alt} />
            </TabPanel>
            <TabPanel value={value} index={1}>
              <img style={{ height: '500px' }} src={`/${imagen2}`} alt={alt} />
            </TabPanel>
            <TabPanel value={value} index={2}>
              <img style={{ height: '500px' }} src={`/${imagen3}`} alt={alt} />
            </TabPanel>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" >
              <Grid item xs justifyContent="left" alignItems="left">
                <Typography gutterBottom variant="h2">
                  { name }
                </Typography>
                <Typography color="textSecondary">ID: {id}</Typography>
                <Typography variant="subtitle1">Categoria: {categoria}</Typography>
                <Typography variant="subtitle1">Stock: {stock}</Typography>
                <Typography variant="h3">
                <NumberFormat value={precio}
                                className="foo"
                                displayType={'text'}
                                thousandSeparator={true}
                                prefix={'$'}
                                renderText={(value, props) => <div {...props}>{value}</div>}
                />
                </Typography>
                <Typography sx={{ pt: '15px' }} gutterBottom>{description}</Typography>
                <Typography sx={{ pt: '15px' }} gutterBottom>{descriptionLong}</Typography>
                <Paper sx={{ margin: 'auto', backgroundColor: '#fff3cc' }}>
                  <Grid container direction="row" justifyContent="center" alignItems="center" spacing={0}>
                    <Button onClick={addItem} variant='contained' color='secondary' sx={{ p: 1, width: '10%' }} size="small">+</Button>
                    <h2 style={{ padding: '10px' }}>{count}</h2>
                    <Button onClick={quitItem} variant='contained' color='secondary' sx={{ p: 1, width: '10%' }} size="small">-</Button>
                    <Button onClick={onAdd} variant='contained' color='primary' sx={{ m: 1, width: '32.5%' }} size="medium">Agregar</Button>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
    </div>
  )
}

export default ItemDetail
