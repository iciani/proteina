import React from 'react'
import Item from './Item'
import { Grid } from '@mui/material'
import MiLoader from './MiLoader'
import Title from './Title'

const ItemList = ({ productos, loading, setCurrentItem }) => {
  return (
        <Grid container direction="row" justifyContent="space-around" alignItems="center" spacing={0} sx={{ mt: 2 }}>
            { loading && [...Array(4)].map((x, i) => {
              return (
                <MiLoader key={i + 1} />
              )
            })}

            {productos && !loading && productos.map(producto => {
              return (
                <Grid item key={producto.id} xs={6} sm={6} md={6} lg={2} sx={{ p: 2 }}>
                    <Item id={producto.id}
                            name={producto.name}
                            alt={producto.alt}
                            precio={producto.precio}
                            imagen={producto.imagen}
                            stock={producto.stock}
                            initial={producto.initial}
                            description={producto.description}
                            categoria={producto.categoria}
                            setCurrentItem={setCurrentItem} />
                </Grid>
              )
            })}

            {productos && !loading && productos.length === 0 &&
                <Title name="No hay ITEMS."/>
            }
        </Grid>
  )
}

export default ItemList
