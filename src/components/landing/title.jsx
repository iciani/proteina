import React from 'react'
import Typography from '@mui/material/Typography'
import '../../styles.css'

const Title = ({ name }) => {
  return (
    <Typography variant="h4" sx={{ pt: 3 }}>
      {name}
    </Typography>
  )
}

export default Title
