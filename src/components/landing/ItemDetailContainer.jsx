import React, { useState, useEffect } from 'react'

import '../../styles.css'
import ItemDetail from './ItemDetail'
import { useParams } from 'react-router-dom'
// import getProducts from '../../services/handMadePromise'
// import { getFirestore } from '../../firebase'
import { doc, getDoc, getFirestore } from 'firebase/firestore'
import { Paper } from '@mui/material'

const ItemDetailContainer = () => {
  const { itemID } = useParams()
  const [item, setItem] = useState()

  useEffect(() => {
    const db = getFirestore()
    const producto = doc(db, 'productos', itemID)

    getDoc(producto).then((snapshot) => {
      if (snapshot.exists()) {
        setItem({ ...snapshot.data(), id: snapshot.id })
      }
    }).catch(
      // (error) => {
      // console.log('Error al conectar con firebase en ItemDetailContainer.'.error)
      // throw new Error('Whoops!')
      // }
    )
  }, [itemID])

  return (
        <>
          <Paper sx={{ mt: '15px', mr: '2px', ml: '5px', minHeight: '600px' }}>
            {item && <ItemDetail key={item.id}
                                id={item.id}
                                name={item.name}
                                alt={item.alt}
                                imagen={item.imagen}
                                imagen2={item.imagen2}
                                imagen3={item.imagen3}
                                stock={item.stock}
                                initial={item.initial}
                                description={item.description}
                                descriptionLong={item.descriptionLong}
                                categoria={item.categoria}
                                precio={item.precio}
                      />
              }
            </Paper>
        </>
  )
}

export default ItemDetailContainer
