import * as React from 'react'
import { Paper, TextField, Box, Button } from '@mui/material'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import TextareaAutosize from '@mui/material/TextareaAutosize'

import Typography from '@mui/material/Typography'
import '../../styles.css'

function Contacto () {
  // const [tipo, setTipo] = React.useState('')

  const handleChange = (event) => {
    // setTipo(event.target.value)
  }

  const onSubmit = (event) => {
    // setTipo(event.target.value)
  }

  return (
    <div id="contacto" style={{ minHeight: '700px' }}>
      <div className="welcome" style={{ maxHeight: '500px' }}>
        <Typography variant="h2" color="white" sx={{ pt: 5 }}>Contacto</Typography>
        <Paper sx={{ mt: 5, mr: '25%', ml: '25%', pl: '5', pt: 5, pr: 5, pb: 5, borderRadius: 0 }}>
            <Box component="form" sx={{ '& .MuiTextField-root': { m: 1, width: '80%' } }} autoComplete="off">
                <div>
                    <TextField label="Nombre" variant="standard"/>
                    <TextField label="Apellido" variant="standard"/>
                </div>
                <div>
                    <TextField label="Mail" variant="standard" />
                    <FormControl variant="standard" sx={{ m: 1, width: '80%' }}>
                        <InputLabel id="tipo">Tipo de Contacto</InputLabel>
                        <Select labelId="tipo" onChange={handleChange} label="Tipo de Contacto">
                            <MenuItem value=""><em></em></MenuItem>
                            <MenuItem value={'reclamo'}>Reclamo</MenuItem>
                            <MenuItem value={'consulta'}>Consulta</MenuItem>
                            <MenuItem value={'info'}>Info sobre pedidos</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <div>
                    <TextareaAutosize placeholder="Deje aqui su mensaje..." aria-label="minimum height" minRows={10} style={{ width: '80%' }} />
                </div>
                <Button onClick={onSubmit} variant='contained' sx={{ width: '81%' }} size="medium">Enviar   </Button>
            </Box>
        </Paper>
      </div>
    </div>
  )
}

export default Contacto
