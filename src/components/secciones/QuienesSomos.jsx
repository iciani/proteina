import * as React from 'react'
import { Grid, Paper } from '@mui/material'
import Typography from '@mui/material/Typography'
import '../../styles.css'

function QuienesSomos () {
  return (
    <div id="quienesSomos" style={{ minHeight: '700px' }}>
      <div className="welcome" style={{ maxHeight: '500px' }}>
        <Typography variant="h2" color="white" sx={{ pt: 5 }}>Quienes Somos</Typography>
        <Paper sx={{ mt: 5, mr: '15%', ml: '15%', pl: '5', pt: 5, pr: 5, pb: 5, borderRadius: 0 }}>

          <Grid container direction="row" alignItems="center" justifyContent="center" spacing={0} sx={{ mt: 2 }}>
            <Grid item xs={12} sm={3} md={3} lg={3} sx={{ p: 2 }}>
              <img src="arnold.png" alt="Arnold-Schuarzeneger" style={{ height: '15em' }} />
            </Grid>
            <Grid item xs={11} sm={11} md={7} lg={7} alignItems="flex-start">
              <Typography align="left" variant="body2" color="text.secondary">
                Es aquí y solo aquí donde le vas a demostrar a las personas que visitan tu web que detrás de esa pantalla hay individuos reales, no máquinas que trabajan día a día para que su producto o servicio esté a su altura.
                Esta, por lo general; es la sección más visitada y pues la razón te la acabo de decir, las personas buscan conexión, saber que hay alguien detrás de la pantalla que siempre estará ahí para resolver sus problemas es suficiente para ellos.
              </Typography>
            </Grid>
          </Grid>

          <Grid container direction="row" alignItems="center" justifyContent="center" spacing={0} sx={{ mt: 2 }}>
            <Grid item xs={11} sm={11} md={8} lg={8}>
              <Typography align="right" variant="body2" color="text.secondary" sx={{ mr: 3 }}>
                Es en esta sección donde debes hablar de lo que haces, de tus valores y tus principios. Cuéntales tu historia y hazlos parte de ella, conecta sentimientos. Humaniza tu marca. Y, si tu equipo está formado por alguien más que tú, por lo que más quieras PRESÉNTALOS. Tómales una foto y escribe algo lindo sobre ellos y sus capacidades profesionales, que tus usuarios vean que el valor del trabajo en equipo no falta dentro de tu compañía.
                Sin embargo es necesario que sepas que no vas a escribir una biografía de cada uno, solo con resaltar su capacidades laborales y algunos aspectos personales es suficiente.
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12} md={3} lg={3} sx={{ p: 2 }}>
              <img src="mancuerna.png" alt="Mancuerna" style={{ height: '10em' }} />
            </Grid>
          </Grid>
        </Paper>
      </div>
    </div>
  )
}

export default QuienesSomos
