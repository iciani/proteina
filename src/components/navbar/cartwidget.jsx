import * as React from 'react'
import IconButton from '@mui/material/IconButton'
import Badge from '@mui/material/Badge'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import { useContext } from 'react'
import { CartContext } from '../../Context/CartContext'
import { Link } from 'react-router-dom'

const CartWidget = () => {
  const context = useContext(CartContext)

  return (
      <div>
        <Link className="noDecorationForLinks" to="/cart">
          <IconButton size="large" aria-label="show articles" color="inherit">
            <Badge badgeContent={context.count} color="error">
              <ShoppingCartIcon />
            </Badge>
          </IconButton>
        </Link>
      </div>
  )
}

export default CartWidget
