import * as React from 'react'
import { styled, alpha } from '@mui/material/styles'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'
import InputBase from '@mui/material/InputBase'
import MenuItem from '@mui/material/MenuItem'
import Menu from '@mui/material/Menu'
import SearchIcon from '@mui/icons-material/Search'
import AccountCircle from '@mui/icons-material/AccountCircle'
import MoreIcon from '@mui/icons-material/MoreVert'
import CartWidget from './CartWidget'
import { Link } from 'react-router-dom'
import '../../styles.css'

const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25)
  },
  marginRight: theme.spacing(2),
  marginLeft: 0,
  width: '100%',
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(3),
    width: 'auto'
  }
}))

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center'
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: '20ch'
    }
  }
}))

export default function NavBar () {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const [anchorCatEl, setAnchorCatEl] = React.useState(null)
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null)

  const isMenuOpen = Boolean(anchorEl)
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null)
  }

  const handleMenuClose = () => {
    setAnchorEl(null)
    handleMobileMenuClose()
  }

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget)
  }

  const menuId = 'primary-search-account-menu'
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Tu Cuenta</MenuItem>
      <MenuItem onClick={handleMenuClose}>Log Out</MenuItem>
    </Menu>
  )

  const mobileMenuId = 'primary-search-account-menu-mobile'
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right'
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  )

  const openCategoryMenu = Boolean(anchorCatEl)
  const handleCategoryClick = e => {
    setAnchorCatEl(e.currentTarget)
  }

  const handleCategoryClose = e => {
    setAnchorCatEl(null)
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Link className="noDecorationForLinks" to="/"><img src="proteina_full.png" alt="logo"/></Link>
          <Link className="noDecorationForLinks" to="/">
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ display: { xs: 'none', sm: 'block' } }}
            >
            PROTEINA
            </Typography>
          </Link>
          <Search>
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Buscar..."
              inputProps={{ 'aria-label': 'search' }}
            />
          </Search>
          <Link className="noDecorationForLinks" to="/">
            <Typography
              sx={{ marginRight: '20px', cursor: 'pointer' }}
              aria-label="Home del E-Commerce"
              color="inherit"
            >
              Home
            </Typography>
          </Link>

          <Typography
            sx={{ marginRight: '20px', cursor: 'pointer' }}
            aria-label="categorias del e-commerce"
            aria-controls="basic-menu"
            aria-haspopup="true"
            aria-expanded={openCategoryMenu ? 'true' : undefined}
            onClick={handleCategoryClick}
            color="inherit"
          >
            Categorias
          </Typography>

          <Link className="noDecorationForLinks" to="/quienes">
            <Typography
              sx={{ marginRight: '20px', cursor: 'pointer' }}
              aria-label="Quienes Somos"
              color="inherit"
            >
              Quienes Somos
            </Typography>
          </Link>

          <Link className="noDecorationForLinks" to="/contacto">
            <Typography
              sx={{ marginRight: '20px', cursor: 'pointer' }}
              aria-label="Contacto"
              color="inherit"
            >
              Contacto
            </Typography>
          </Link>

          {/* dropdown items for Categories */}
          <Menu id="basic-menu" anchorEl={anchorCatEl} open={openCategoryMenu} onClose={handleCategoryClose}>
              <MenuItem><Link className="noDecorationForLinks" to="/proteinas">Proteinas</Link></MenuItem>
              <MenuItem><Link className="noDecorationForLinks" to="/vitaminas">Vitaminas</Link></MenuItem>
              <MenuItem><Link className="noDecorationForLinks" to="/quemadores">Quemadores</Link></MenuItem>
              <MenuItem><Link className="noDecorationForLinks" to="/pre-workouts">Pre-Workout</Link></MenuItem>
              <MenuItem><Link className="noDecorationForLinks" to="/post-workouts">Post-Workout</Link></MenuItem>
          </Menu>
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
            <CartWidget />
            <IconButton
              size="large"
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </Box>
          <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
            <IconButton
              size="large"
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </Box>
  )
}
