import * as React from 'react'
import Button from '@mui/material/Button'
import NumberFormat from 'react-number-format'
import ButtonGroup from '@mui/material/ButtonGroup'
import Badge from '@mui/material/Badge'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import AddIcon from '@mui/icons-material/Add'
import RemoveIcon from '@mui/icons-material/Remove'
import DeleteForeverIcon from '@mui/icons-material/DeleteForever'
import TableCell from '@mui/material/TableCell'
import TableRow from '@mui/material/TableRow'
import { useContext } from 'react'
import { CartContext } from '../../Context/CartContext'
import Swal from 'sweetalert2'
// import withReactContent from 'sweetalert2-react-content'

function CartDetail ({ product }) {
  const context = useContext(CartContext)
  const [itemCount, setItemCount] = React.useState(parseInt(product.quantity))

  const addItem = () => {
    setItemCount(parseInt(itemCount) + 1)
    context.addItem(product.id, product.name, product.alt, product.precio, 1)
  }

  const quitItem = () => {
    if (itemCount - 1 > 0) {
      setItemCount(Math.max(parseInt(itemCount) - 1, 1))
      context.addItem(product.id, product.name, product.alt, product.precio, -1)
    }
  }

  const removeItem = () => {
    context.removeItem(product.id)

    Swal.fire({
      position: 'center',
      icon: 'error',
      title: 'Producto removido del Carrito.',
      showConfirmButton: false,
      timer: 750
    })
  }

  return (
        <TableRow key={product.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell component="th" scope="row">{product.id}</TableCell>
            <TableCell>{product.name}</TableCell>
            <TableCell>{product.alt}</TableCell>
            <TableCell align="right">{product.quantity}</TableCell>
            <TableCell align="right" sx={{ fontWeight: 'bold' }}>
                <NumberFormat value={product.precio}
                              displayType={'text'}
                              thousandSeparator={true}
                              prefix={'$'}
                              renderText={(value, props) => <div {...props}>{value}</div>} />
            </TableCell>
            <TableCell align="right" sx={{ fontWeight: 'bold' }}>
                <NumberFormat value={product.precio * product.quantity}
                              displayType={'text'}
                              thousandSeparator={true}
                              prefix={'$'}
                              renderText={(value, props) => <div {...props}>{value}</div>} />
            </TableCell>
            <TableCell align="right">
                <Badge color="secondary" badgeContent={itemCount}><ShoppingCartIcon />{' '}</Badge>
                <ButtonGroup>
                    <Button onClick={quitItem}>{' '}<RemoveIcon fontSize="small" /></Button>
                    <Button onClick={addItem}>{' '}<AddIcon fontSize="small" /></Button>
                    <Button onClick={removeItem}>{' '}<DeleteForeverIcon fontSize="small" sx={{ color: 'red' }} /></Button>
                </ButtonGroup>
            </TableCell>
        </TableRow>
  )
}

export default CartDetail
