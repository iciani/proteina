import * as React from 'react'
import { Paper, /** TextField,**/ Box, Typography } from '@mui/material'
import ShopIcon from '@mui/icons-material/Shop'
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft'
import { CartContext } from '../../Context/CartContext'
import { useContext } from 'react'
import Button from '@mui/material/Button'
import FormControl from '@mui/material/FormControl'
import { useFormik } from 'formik'
// import InputLabel from '@mui/material/InputLabel'
// import MenuItem from '@mui/material/MenuItem'
// import Select from '@mui/material/Select'
// import FormHelperText from '@mui/material/FormHelperText'
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import NumberFormat from 'react-number-format'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableRow from '@mui/material/TableRow'
import { Link } from 'react-router-dom'
import { collection, getFirestore, addDoc, writeBatch, increment, doc } from 'firebase/firestore'
import Swal from 'sweetalert2'
import '../../styles.css'

import InputContainer from '../InputContainer'
import dataForm from './checkoutForm'

function CartCheckout () {
  const context = useContext(CartContext)

  const formik = useFormik({
    // Inicializamos los campos que va a tener el formulario. Es requerido por la libreria FORMIK.
    initialValues: { name: '', surname: '', dni: '', phone: '', email: '', dir: '', dir2: '', provincia: '', localidad: '', cp: '' },

    // Mapeamos las expresiones regulares que validaran cada input.
    validate: (values) => {
      const errors = {}

      dataForm.map((input) => {
        const reg = new RegExp(input.regex, 'i')

        if (input.required && !values[input.id]) {
          errors[input.id] = 'Dato Requerido.'
        } else if (input.regex !== '' && !reg.test(values[input.id])) {
          errors[input.id] = 'Formato inválido.'
        }
        return true
      })

      return errors
    },

    onSubmit: (values) => {
      Swal.fire({
        title: 'Procesando...',
        allowOutsideClick: false,
        color: '#716add',
        backdrop: 'rgba(0,0,123,0.4)',
        showCancelButton: false,
        showConfirmButton: false,
        onBeforeOpen: () => {
          Swal.showLoading()
        }
      })

      // Generamos el objeto con todo el contenido pertinente.
      const order = {
        orderBuyer: values,
        orderItems: context.products,
        orderTotal: context.total,
        orderDate: Date.now()
      }

      // Conectamos a Firestore y guardamos referencia a la coleccion "ORDERS".
      const db = getFirestore()
      const ordersCollection = collection(db, 'orders')

      // Generamos la Orden en FIRESTORE.
      addDoc(ordersCollection, order).then(({ id }) => {
        Swal.fire({
          title: 'Orden creada con éxito.',
          icon: 'success',
          iconColor: '#ffd740',
          html: `Id de la Orden: <b>${id}</b>`,
          width: 600,
          padding: '3em',
          confirmButtonColor: '#ffd740'
        })
      })

      const batch = writeBatch(db)

      // Recorremos producto a producto de los recientemente comprados, para su update de stock.
      context.products.forEach(function (prodCart) {
        const orderDoc = doc(db, 'productos', prodCart.id)
        batch.update(orderDoc, { stock: increment(-prodCart.quantity) })
      })

      // Commitiamos los cambios del batch update.
      batch.commit()

      // Vaciamos el carrito una vez que se genero la orden.
      context.clear()
    }
  })

  return (
    <Paper sx={{ mt: '15px', mr: '5px', ml: '5px', p: '50px', pl: '15%', pr: '15%', minHeight: '600px' }}>
        <h1>Finalizar Compra:</h1>
        <Box sx={{ display: 'inline-flex', justifyContent: 'space-between' }}>
            <div style={{ border: '1px solid black', padding: '25px', width: '45%', mr: 5 }}>
                <Typography variant="subtitle1" align="left" color="black" sx={{ pb: 0 }}>FACTURACIÓN Y ENVÍO</Typography>
                <hr style={{ width: '50%', marginLeft: 0, marginBottom: 15, padding: 0 }} />
                <div>
                    <form onSubmit={formik.handleSubmit} >
                        {dataForm.map((input) => (
                            <InputContainer key={input.id}
                                  tipo={input.tipo}
                                  variant={input.variant}
                                  id={input.id}
                                  name={input.name}
                                  label={input.label}
                                  value={formik.values[input.name]}
                                  handleChange={formik.handleChange}
                                  selectValues={input.selectValues}
                                  error={formik.touched[input.name] && Boolean(formik.errors[input.name])}
                                  helperText={formik.touched[input.name] && formik.errors[input.name]}
                            />
                        ))}
                    </form>
                </div>
            </div>
            <div style={{ border: '1px solid black', maxHeight: '550px', padding: '15px', width: '40%' }}>

                <TableContainer>
                    <Table sx={{ width: '100%' }} aria-label="simple table">
                        <TableBody>
                            <TableRow>
                                <TableCell className="tableCheckout">TOTAL</TableCell>
                                <TableCell>
                                    <NumberFormat className="totalCheckout" value={context.total}
                                                  displayType={'text'}
                                                  thousandSeparator={true}
                                                  prefix={'$'}
                                                  renderText={(value, props) => <div {...props}>{value}</div>} />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className="tableCheckout">TOTAL ITEMS</TableCell>
                                <TableCell>{context.count}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className="tableCheckout">MÉTODO DE PAGO</TableCell>
                                <TableCell>
                                    <FormControl component="fieldset">
                                        <RadioGroup aria-label="metodo" defaultValue="Efectivo" name="radio-buttons-group">
                                            <FormControlLabel value="MercadoPago" control={<Radio />} label={<img style={{ width: 100, paddingTop: 10 }} src="/mercadopago.png" alt="Mercado Pago - Tarjeta de Credito"></img>} />
                                            <FormControlLabel value="Efectivo" control={<Radio />} label="Efectivo" />
                                        </RadioGroup>
                                    </FormControl>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell className="tableCheckout">MÉTODO DE ENVÍO</TableCell>
                                <TableCell>
                                    <FormControl component="fieldset">
                                        <RadioGroup aria-label="envio" defaultValue="Retiro" name="radio-buttons-group">
                                            <FormControlLabel value="Retiro" control={<Radio />} label="Retiro en Persona" />
                                            <FormControlLabel value="OCA" control={<Radio />} label="Envio por OCA" />
                                        </RadioGroup>
                                    </FormControl>
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
                <Link className="noDecorationForLinks" to="/cart">
                    <Button fullWidth variant="contained" color="secondary" sx={{ color: 'white', mr: 5, mt: 2 }}>
                        <ArrowLeftIcon fontSize="large" sx={{ pr: 1 }} /> Carrito
                    </Button>
                </Link>
                <Button disabled={context.count === 0} fullWidth onClick={formik.handleSubmit} variant="contained" sx={{ color: 'black', mt: 2 }}>
                    <ShopIcon fontSize="large" sx={{ pr: 1 }} /> Realizar Pedido
                </Button>
            </div>
        </Box>
    </Paper>
  )
}

export default CartCheckout
