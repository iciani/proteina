/* eslint-disable */
const dataForm = [
  {
    tipo: 'text',
    variant: 'standard',
    id: 'name',
    name: 'name',
    label: 'Nombre',
    required: true,
    regex: "^[A-Z]*$"
  },
  {
    tipo: 'text',
    variant: 'standard',
    id: 'surname',
    name: 'surname',
    label: 'Apellido',
    required: true,
    regex: "^[A-Z]*$"
  },
  {
    tipo: 'text',
    variant: 'standard',
    id: 'dni',
    name: 'dni',
    label: 'DNI',
    required: true,
    regex: "^([0-9-.'&])*$"
  },
  {
    tipo: 'text',
    variant: 'standard',
    id: 'phone',
    name: 'phone',
    label: 'Teléfono',
    required: false,
    regex: ''
  },
  {
    tipo: 'text',
    variant: 'standard',
    id: 'email',
    name: 'email',
    label: 'Email',
    required: true,
    regex: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$"
  },
  {
    tipo: 'text',
    variant: 'standard',
    id: 'dir',
    name: 'dir',
    label: 'Dirección',
    required: true,
    regex: ''
  },
  {
    tipo: 'text',
    variant: 'standard',
    id: 'dir2',
    name: 'dir2',
    label: 'Piso/Dpto',
    required: false,
    regex: ''
  },
  {
    tipo: 'select',
    variant: 'standard',
    id: 'provincia',
    name: 'provincia',
    label: 'Provincia',
    selectValues: [
                   { value: 'Buenos Aires', text: 'Buenos Aires' },
                   { value: 'Capital Federal', text: 'Capital Federal' }
                  ],
    required: false,
    regex: ''
  },
  {
    tipo: 'text',
    variant: 'standard',
    id: 'localidad',
    name: 'localidad',
    label: 'localidad',
    required: false,
    regex: ''
  },
  {
    tipo: 'text',
    variant: 'standard',
    id: 'cp',
    name: 'cp',
    label: 'cp',
    required: false,
    regex: ''
  }
]

export default dataForm
