import * as React from 'react'
import { Paper } from '@mui/material'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import CreditScoreIcon from '@mui/icons-material/CreditScore'
import { CartContext } from '../../Context/CartContext'
import CartDetail from './CartDetail'
import { useContext } from 'react'
import NumberFormat from 'react-number-format'
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft'
import DeleteIcon from '@mui/icons-material/Delete'
import Button from '@mui/material/Button'
import { Link, useNavigate } from 'react-router-dom'
import '../../styles.css'

function CartDetailContainer () {
  const context = useContext(CartContext)
  const navigate = useNavigate()

  function handleComprarClick () {
    if (context.count > 0) {
      navigate('/checkout', { replace: true })
    }
  }

  return (
    <Paper sx={{ mt: '15px', mr: '5px', ml: '5px', p: '50px', pl: '15%', pr: '15%', minHeight: '600px' }}>
        <h1>Detalle de Productos en el Carrito:</h1>
        <div style={{ border: '2px solid black', minHeight: '350px', padding: '15px' }}>
            { context.products.length > 0
              ? <TableContainer>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell><b>Id</b></TableCell>
                            <TableCell><b>Nombre Producto</b></TableCell>
                            <TableCell><b>Marca</b></TableCell>
                            <TableCell align="right"><b>Cantidad</b></TableCell>
                            <TableCell align="right"><b>Precio U.</b></TableCell>
                            <TableCell align="right"><b>Total</b></TableCell>
                            <TableCell align="right"><b>Acciones</b></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {context.products.map((product) => (
                            <CartDetail key={product.id} product={product} />
                        ))}

                        <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            <TableCell component="th" scope="row"></TableCell>
                            <TableCell></TableCell>
                            <TableCell></TableCell>
                            <TableCell align="right"></TableCell>
                            <TableCell align="right" sx={{ fontWeight: 'bold' }}>TOTAL</TableCell>
                            <TableCell align="right" sx={{ fontWeight: 'bold' }}>
                                <NumberFormat value={context.total}
                                                displayType={'text'}
                                                thousandSeparator={true}
                                                prefix={'$'}
                                                renderText={(value, props) => <div {...props}>{value}</div>} />
                            </TableCell>
                            <TableCell align="right"></TableCell>
                        </TableRow>

                    </TableBody>
                </Table>
            </TableContainer>
              : <>
                <h3>No Hay Elementos en el Carrito.</h3>
                <Link className="noDecorationForLinks" to="/">
                    <Button variant="outlined" sx={{ color: 'black' }}>
                        <ArrowLeftIcon fontSize="medium" />Volver al Home
                    </Button>
                </Link>
              </>
            }
        </div>
        <hr/>
        <Button disabled={context.count === 0} onClick={() => context.clear()} variant="contained" color="secondary" sx={{ color: 'black', mr: 5 }} >
          <DeleteIcon fontSize="large" sx={{ pr: 1 }} /> Vaciar Carrito
        </Button>
        <Button disabled={context.count === 0} onClick={() => handleComprarClick()} variant="contained" sx={{ color: 'black' }} >
          <CreditScoreIcon fontSize="large" sx={{ pr: 1 }} /> Finalizar Compra
        </Button>
    </Paper>
  )
}

export default CartDetailContainer
