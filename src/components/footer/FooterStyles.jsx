import styled from 'styled-components'

export const Box = styled.div`
  background: black;
  bottom: 0;
  width: 100%;
`

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    //max-width: 1000px;
    margin: 0 auto;
    /* background: red; */
`

export const FooterLink = styled.a`
  color: #fff;
  margin-bottom: 20px;
  font-size: 18px;
  text-decoration: none;
   
  &:hover {
      color: #ffd740;
      transition: 200ms ease-in;
  }
`

export const Heading = styled.p`
  font-size: 24px;
  color: #fff;
  margin-bottom: 40px;
  font-weight: bold;
`

export const Content = styled.p`
  font-size: 18px;
  color: #fff;
`
