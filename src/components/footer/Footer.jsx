import React from 'react'
import {
  Box,
  Container,
  FooterLink,
  Heading,
  Content
} from './FooterStyles'
import { Grid } from '@mui/material'

const Footer = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <h1 style={{
        color: '#ffd740',
        textAlign: 'center',
        marginTop: '90px',
        paddingTop: '20px'
      }}>
        Proteina, un E-Commerce con mucha fuerza
      </h1>
      <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 3, sm: 6, md: 12 }}>
          <Grid item xs={3} sm={3} md={3}>
            <Container>
              <Heading>Nosotros</Heading>
              <FooterLink href="#">Objetivos</FooterLink>
              <FooterLink href="#">Vision</FooterLink>
              <FooterLink href="#">Opiniones</FooterLink>
            </Container>
          </Grid>
          <Grid item xs={3} sm={3} md={3}>
            <Content>
              <Heading>Contacto</Heading>
                Vendemos accesorios, indumentaria y<br />
                suplementos para todo tipo de deportes.<br />
                Venta por mayor y por menor.<br />
                Casa central: 011 4216 5114<br />
                E-mail: info@demusculos.com
            </Content>
          </Grid>
          <Grid item xs={3} sm={3} md={3}>
            <Heading>Locales</Heading>
            <Content>
              Dorrego AV. 2121, 1ro &apos;A&apos;<br />
              Capital Federal, CABA,<br />
              Argentina
            </Content>
          </Grid>
          <Grid item xs={3} sm={3} md={3}>
            <Container>
              <Heading>Social Media</Heading>
              <FooterLink href="#">
                <i className="fab fa-instagram">
                  <span style={{ marginLeft: '10px' }}>
                    Instagram
                  </span>
                </i>
              </FooterLink>
              <FooterLink href="#">
                <i className="fab fa-twitter">
                  <span style={{ marginLeft: '10px' }}>
                    Twitter
                  </span>
                </i>
              </FooterLink>
              <FooterLink href="#">
                <i className="fab fa-youtube">
                  <span style={{ marginLeft: '10px' }}>
                    Youtube
                  </span>
                </i>
              </FooterLink>
            </Container>
          </Grid>
      </Grid>
    </Box>
  )
}
export default Footer
