import * as React from 'react'
import { createContext, useState } from 'react'

export const CartContext = createContext()

export const CartProvider = ({ children }) => {
  const [products, setProducts] = useState([])
  const [count, setCount] = useState(0)
  const [total, setTotal] = useState(0)

  // AGREGAMOS UN ITEM AL CART.
  const addItem = (id, name, alt, precio, quant) => {
    let cartLine = {
      id: id,
      name: name,
      alt: alt,
      precio: precio,
      quantity: parseInt(quant)
    }

    if (!isInCart(id)) {
      setProducts([...products, cartLine])
    } else {
      // Copiamos el array actual.
      const result = [...products]

      // Traigo el producto.
      cartLine = products.find((element) => { return element.id === id })

      // Buscamos el INDEX del objeto que vamos a modificar en el array.
      const index = products.findIndex((element) => { return element.id === id })

      // Le sumo la cantidad que acaba de agregar el usuario.
      cartLine.quantity += parseInt(quant)

      result[index] = cartLine

      // Vuelvo a meter el producto en el Array de Productos.
      setProducts(result)
    }

    // Sumarizamos el contador de articulos en carrito.
    setCount(parseInt(count) + parseInt(quant))

    // Calculamos el total que va llevando el carrito en $.
    setTotal(total + (quant * precio))
  }

  // REMOVEMOS UN ITEM EN PARTICULAR DEL CART.
  const removeItem = (id) => {
    const result = [...products]

    // Buscamos el INDEX del objeto que vamos a borrar en el array.
    const index = result.findIndex((element) => { return element.id === id })

    // Restamos del total, el valor del itema que se esta borrando.
    setTotal(total - result[index].precio * result[index].quantity)

    // Restamos del contador de items, los que se acaban de quitar.
    setCount(count - result[index].quantity)

    // Chequeamos por las dudas, que realmente el producto exista en el carrito.
    if (index !== -1) {
      result.splice(index, 1)
    }

    setProducts(result)
  }

  // REMOVEMOS TODOS LOS ITEMS DEL CART.
  const clear = () => {
    // Seteamos un arreglo vacio en el Products. Reseteamos el Counter de Productos.
    setProducts([])
    setCount(0)
  }

  // Preguntamos si el ITEM ya existe en el carrito.
  const isInCart = (id) => {
    return products.find((element) => { return element.id === id })
  }

  const value = { addItem, removeItem, clear, total, products, count }

  return (
        <CartContext.Provider value={value}>
            {children}
        </CartContext.Provider>
  )
}
