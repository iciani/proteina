import './App.css'
import NavBar from './components/navbar/NavBar'
import ItemListContainer from './components/landing/ItemListContainer'
import ItemDetailContainer from './components/landing/ItemDetailContainer'
import { ThemeProvider } from '@emotion/react'
import { createTheme } from '@mui/material'
import { CartProvider } from './Context/CartContext'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Footer from './components/footer/Footer'
import { useState } from 'react'
import CartDetailContainer from './components/cart/CartDetailContainer'
import QuienesSomos from './components/secciones/QuienesSomos'
import Contacto from './components/secciones/Contacto'
import CartCheckout from './components/cart/CartCheckout'

const styles = {
  paperContainer: {
    height: 900,
    backgroundImage: 'url("/main.png")'
  }
}

const theme = createTheme({
  type: 'light',
  palette: {
    primary: {
      main: '#ffd740'
    },
    secondary: {
      main: '#5F5F5F'
    }
  },
  overrides: {
    MuiButton: {
      raisedPrimary: {
        color: 'white'
      }
    }
  },
  typography: {
    fontFamily: 'Do Hyeon',
    fontSize: 18
  },
  shape: {
    borderRadius: 10
  }
})

function App () {
  const [currentItem, setCurrentItem] = useState({})

  return (

    <ThemeProvider theme={theme}>
      <div style={styles.paperContainer} className="App">
        <CartProvider>
          <BrowserRouter>
            <NavBar/>
            <Routes>
              <Route path="/" element={<ItemListContainer greeting="Ofertas de la Semana!" inicio={true} setCurrentItem={setCurrentItem} />}></Route>
              <Route path="/proteinas" element={<ItemListContainer greeting="Proteinas" inicio={false} setCurrentItem={setCurrentItem} />}></Route>
              <Route path="/vitaminas" element={<ItemListContainer greeting="Vitaminas" inicio={false} setCurrentItem={setCurrentItem} />}></Route>
              <Route path="/quemadores" element={<ItemListContainer greeting="Quemadores" inicio={false} setCurrentItem={setCurrentItem} />}></Route>
              <Route path="/post-workouts" element={<ItemListContainer greeting="Post-Workouts" inicio={false} setCurrentItem={setCurrentItem} />}></Route>
              <Route path="/pre-workouts" element={<ItemListContainer greeting="Pre-Workouts" inicio={false} setCurrentItem={setCurrentItem} />}></Route>
              <Route path="/item/:itemID" element={<ItemDetailContainer currentItem={currentItem} />}></Route>
              <Route path="/cart" element={ <CartDetailContainer/> }></Route>
              <Route path="/quienes" element={ <QuienesSomos/> }></Route>
              <Route path="/contacto" element={ <Contacto/> }></Route>
              <Route path="/checkout" element={ <CartCheckout/> }></Route>
            </Routes>
          </BrowserRouter>
          <Footer/>
        </CartProvider>
      </div>
    </ThemeProvider>
  )
}

export default App
