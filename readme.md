# ¡PROTEINA E-COMMERCE!

# URL (Productivo)

http://www.buyproteina.com.ar

# Introducción

Este es mi primer proyecto con React. A continuacion, describo lo utilizado durante el proyecto, para lograr algunas caracteristicas **copadas!**.

# Ejecucion del PROYECTO

Para ejecutar el mismo, es requerido correr el comando **npm run start:staging**. Se utilizo la libreria env-cmd para mover las API KEYS a un archivo .env. De esta forma no compartis las credenciales en etapas de desarrollo.

# Directorio de Archivos

Si bien la documentación de **REACT** oficial, nos dice que no prestemos ni demoremos mucho tiempo en establecer algun razonamiento en el armado del directorio de archivos, me parece una parte fundamental para el entendimiento de un proyecto claro y organizado.

|            | Carpetas    | Contenido                                    |
| ---------- | ----------- | -------------------------------------------- |
| Components | `Cart`      | CartCheckout, CartDetail,CartDetailContainer |
|            | `Footer`    | Footer, FooterStyles                         |
|            | `Navbar`    | CartWidget, Navbar                           |
|            | `Secciones` | Contacto, QuienesSomos                       |
|            | `Landing`   | Item, ItemDetail, ItemDetailContainer,       |
|            | `Landing`   | ItemList, ItemListContainer, MiLoader, Title |
| Context    |             | CartContext                                  |
| Images     |             | Background_gym.jpg                           |
| Styles     |             | styles.css                                   |

## 3rd Party Librerias

A lo largo del curso fuimos aprendiendo sobre diversas librerias que se podran observar instaladas en el package.json. Algunas de ellas fueron comentadas en clase, otras no tanto. A continuacion las describimos:

- **Formik**: Una de las mas famosas librerias para el manejo de formularios. Algo pesada, pero bastante clara.
- **StyledComponents**: Si bien se usaron hojas de estilo tradicionales, investigamos un poco el uso de StyledC.
- **Axios**: Si bien no fue utilizada para consumir ninguna API, se instalo e investigo de forma academica.
- **MUI**: Libreria de estilos utilizada para el proyecto. Muy Copada!
- **Firebase**: Libreria para el manejo de conexiones hacia FireStore. Base de datos NO SQL (No Relacional)
- **react-content-loader**: Para manipular los SVG que aparecen al momento de cargar una vista.
- **react-number-format**: Para formatear de forma clara y rapida los montos de dinero mostrados en las vistas.
- **react-router-dom**: Para manejar el routing de la aplicacion web.
- **sweetalert2**: Libreria para mostrar "ALERTS" copados, popups, loaders, etc. Muy Buena!!!

## Descripción de Alcance

A lo largo de la aplicacion "PROTEINA" se lograron algunas caracteristicas vistas en clase. Manejo de Hooks tales como useState, useEffect, y useContext. Al mismo tiempo, se genero conocimiento en como estilar y manipular componentes react. Si bien falta mucho trabajo en la maquetación, se logro manipular FireStore para Update, Modify, y Creation.

Se seleccionan los "documents" que representan los productos, y se generan las "orders" al momento de comprar. Al mismo tiempo, el stock de los productos es modificado por medio del metodo **"INCREMENT"** de firebase.

## Dependencias de Desarrollo

Se configuro ESlint (Perfil Estandard) y Prettier para lograr un mejor orden e identado de código.
Al mismo tiempo, se configuraron .env files para proteger las credenciales de Firebase, utilizando la libreria **ENV-CMD**.
